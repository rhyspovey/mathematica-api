(* ::Package:: *)

(* ::Title:: *)
(*API interfacing*)


(* ::Text:: *)
(*Rhys G. Povey*)
(*www.rhyspovey.com*)


(* ::Subsection:: *)
(*Change log*)


(* ::Text:: *)
(*21 January 2024*)
(*- Added some additional error handling.*)
(**)
(*6 February 2022*)
(*- More error handling on login.*)
(*- Bug fixes.*)
(**)
(*23 December 2021*)
(*- Massive rework to move specific web services to sub-packages.*)
(*- Improved error handling.*)
(**)
(*14 November 2021*)
(*- Created SpotifyPlayPlaylist and incorporated SpotifyPlayRandomItem.*)
(**)
(*25 August 2021*)
(*- Added SpotifyPlaylistURIs.*)
(*- Added SpotifyRemoveTracks.*)
(*- Added SpotifyReplaceTrack.*)
(*- Added some error handling to Spotify calls using Enclose and Confirm.*)
(**)
(*06 December 2020*)
(*- Removed URLEncode from APILoginURL.*)
(**)
(*20 August 2020*)
(*- Merged fix of YoutubeItem.*)
(*- Fixed SpotifyAddTracks for single track.*)
(**)
(*6 June 2020*)
(*- Fixed bug with untyped optional argument in YoutubeAddToPlaylist.*)
(**)
(*29 April 2020*)
(*- YoutubeAddToPlaylist can now take optional position argument.*)
(**)
(*7 March 2020*)
(*- Fixed bug in YoutubeUpdatePlaylist and YoutubeCreatePlaylist.*)
(**)
(*2 March 2020*)
(*- Added YoutubeItem.*)
(**)
(*12 February 2020*)
(*- Fixed YoutubePlaylistDetails.*)
(**)
(*5 February 2020*)
(*- APIRead now retries any 5xx error once.*)
(**)
(*29 January 2020*)
(*- Added YoutubePlaylistDetails.*)
(*- Improved YoutubePlaylistItems to handle 403 during.*)
(**)
(*18 January 2020*)
(*- Added playing random item in Spotify playlist.*)
(**)
(*29 November 2019*)
(*- Added Spotify playlist creation and playback control.*)
(**)
(*26 October 2019*)
(*- Changed app detail storage to function APIInfo.*)
(**)
(*4 October 2019*)
(*- Bug fixes in default app handling.*)
(**)
(*27 July 2019*)
(*- First version with Spotify and Youtube login support.*)


(* ::Subsection:: *)
(*Sample app info*)


(* ::Text:: *)
(*Default app names, others can be used with the option APIInfo->name.*)
(*Omit 'scope' to use unprivileged tokens.*)


(* ::Input:: *)
(*API["Youtube"]=<|*)
(*"type"->"Youtube",*)
(*"client_id"->"something.apps.googleusercontent.com",*)
(*"client_secret"->"something",*)
(*"redirect_uri"->"urn:ietf:wg:oauth:2.0:oob",*)
(*"scope"->"https://www.googleapis.com/auth/youtube"*)
(*|>;*)


(* ::Input:: *)
(*API["Spotify"]=<|*)
(*"type"->"Spotify",*)
(*"client_id"->"632something",*)
(*"client_secret"->"something",*)
(*"redirect_uri"->"http://www.redirect.com/page.php",*)
(*"scope"->"playlist-modify-public"*)
(*|>;*)


(* ::Text:: *)
(*Additional app details can be made and called using the name.*)


(* ::Section:: *)
(*Front End*)


BeginPackage["API`"];


(* ::Subsection:: *)
(*General*)


API::usage="API[name] stores API details with given name. Functions use option API\[Rule]name to specify.";


ExecuteReponse::usage="ExecuteReponse[HTTPResponse] generates URLExecute output from the HTTPResponse body.";


ReadAuthorizedRequest::usage="ReadAuthorizedRequest[app,HTTPRequest] appends authorization information from app to HTTPRequest and reads.";


APIQ::usage="APIQ[type] checks if API commands for given type are loaded.";


(* ::Subsection:: *)
(*Login*)


APILoginURL::usage="APILoginURL[app] generates the login URL for specified app.";


APIGetToken::usage="APIGetToken[app,code] using code, obtains an access_token and stores it in app. If no code given, attempts to use refresh_token or obtains an unprivileged token.";


APILogin::usage="APILogin[app] provides a dialog for login.";


LoginURL::usage="LoginURL[API(string),details(association)] generates a login URL for API with given details. Needs API subpackage loaded.";


LoginURL::scope="No scope given for login URL. Login is not required for unprivileged access.";


TokenRequest::usage="TokenRequest[API(string),details(association),code] generates token HTTPRequest. Needs API subpackage loaded.";


(* ::Subsection:: *)
(*Read*)


APIRead::usage="APIRead[app,request] does a URLRead, includes authorization, and refreshes tokens if needed. Returns a HTTPResponse.";


APIRead::error="`1` error occured.";


APIRead::notoken="No access_token.";


APIRead::refresh="Refreshing token.";


APIRead::gateway="Retrying.";


APIReadResult::usage="APIReadResult[app,request] performs APIRead and returns either Success or Failure.";


APIReadExecute::usage="APIReadResult[app,request,function] performs APIRead and returns function[ExecuteResponse[response]] on success or Failure if not.";


(* ::Subsection:: *)
(*Error handling*)


ResponseResult::usage="ResponseResult[HTTPResponse] returns Success or Failure.";


HandleExecuteResponse::usage="HandleExecuteResponse[HTTPResponse] executes the response if the call was successful, otherwise returns a Failure.";


(* ::Section:: *)
(*Back end*)


Begin["`Private`"];


(* ::Subsection:: *)
(*General*)


ExecuteReponse[\[FormalX]_HTTPResponse]:=
(* Only for JSON/unencoded, fails for XML *)
Module[{string,words,replacements},
string=\[FormalX]["Body"];
words = StringCases[string,RegularExpression["\".*?\""]]; (* avoid doing replacements inside encapsulated strings *)
replacements={":"->"\[Rule]","["->"{","]"->"}"};
ToExpression@StringJoin[If[#[[1]],StringReplace[#[[2]],replacements],#[[2]]]&/@(If[StringQ@#,{True,#},#]&/@StringSplit[string,\[FormalW]:words:>{False,\[FormalW]}])]
];


ReadAuthorizedRequest[app_String,request_HTTPRequest]:=
URLRead[
If[KeyMemberQ[API@app,"access_token"],
	Join[request,HTTPRequest[<|"Headers"->{"Authorization"->"Bearer "<>Lookup[API@app,"access_token"]}|>],2],
	request
],
Interactive->False];


APIQ[_]=False;


(* ::Subsection:: *)
(*Login*)


APILoginURL[app_String]:=LoginURL[Lookup[API@app,"type"],API@app]; (* use subpackage instructions *)


APIGetToken[app_String,code_:None]:=
Enclose[
Module[{request,read,statuscode,statusdesc,response,newtokens},

request=Confirm@TokenRequest[Lookup[API@app,"type"],API@app,code]; (* use subpackage instructions *)

read=URLRead[request,Interactive->False];
statuscode=read["StatusCode"];
statusdesc=read["StatusCodeDescription"];
response=ExecuteReponse@read;

If[statuscode==200, (* check if successful *)
	
	newtokens=DeleteMissing[(Association@@response)[[{"access_token","refresh_token"}]]];
	AppendTo[API@app,newtokens];
	Success[ToString[code]<>": "<>statusdesc,newtokens],
	
	Failure[ToString[statuscode]<>": "<>statusdesc,<|"Message" -> Lookup[Lookup[response,"error"],"message"]|>]
]
]
];


APILogin[app_String]:=
If[AssociationQ[API[app]],
If[APIQ[Lookup[API@app,"type"]],
DynamicModule[{logincode},
	logincode=Null;
	DialogInput@DialogNotebook[{
		APILoginURL[app],
		InputField[Dynamic@logincode,String,FieldHint->"Access code",FieldSize->{50,2}],
		DefaultButton[DialogReturn[APIGetToken[app,logincode]]]
}]
],
Failure["API",<|"MessageTemplate"->"API commands do not exist for type `type`.","MessageParameters"-><|"type"->Lookup[API@app,"type"]|>|>]
],
Failure["API",<|"MessageTemplate"->"No information stored in API[`app`].","MessageParameters"-><|"app"->app|>|>]
];


(* ::Subsection:: *)
(*Read*)


APIRead[app_String,request_HTTPRequest]:=Module[{read},
(* initial check token exists *)
If[Not@KeyExistsQ[API@app,"access_token"],Message[APIRead::notoken]];

(* read url request *)
read=ReadAuthorizedRequest[app,request];

Which[
	Quotient[read["StatusCode"],100]==2, (* success *)
		read,
	
	read["StatusCode"]==401, (* refresh token, try again *)
		Message[APIRead::refresh];
		APIGetToken[app];Pause[2];read=ReadAuthorizedRequest[app,request],

	Quotient[read["StatusCode"],100]==5, (* bad gateway error, try again once *)
		Message[APIRead::gateway];
		Pause[10];read=ReadAuthorizedRequest[app,request],

	True, (* other error *)
		Message[APIRead::error,read["StatusCode"]];read
]

];


APIReadResult[app_String,request_HTTPRequest,message_String:"None"]:=Module[{read,code,desc},
(* do APIRead *)
read=APIRead[app,request];
code=read["StatusCode"];
desc=read["StatusCodeDescription"];
If[Quotient[code,100]==2,
Success[ToString[code]<>": "<>desc,<|"Message" -> message|>],
Failure[ToString[code]<>": "<>desc,<|"Message" -> Lookup[Lookup[ExecuteReponse[read],"error"],"message"]|>]]
];


APIReadExecute[app_String,request_HTTPRequest,func_:Identity]:=Module[{read,code,desc},
(* do APIRead *)
read=APIRead[app,request];
code=read["StatusCode"];
desc=read["StatusCodeDescription"];
If[Quotient[code,100]==2,
func[ExecuteReponse[read]],
Failure[ToString[code]<>": "<>desc,<|"Message" -> Lookup[Lookup[ExecuteReponse[read],"error"],"message"]|>]]
];


(* ::Subsection:: *)
(*Error handling*)


ResponseResult[\[FormalX]_HTTPResponse]:=If[Quotient[\[FormalX]["StatusCode"],100]===2,
Success[ToString[\[FormalX]["StatusCode"]]<>": "<>\[FormalX]["StatusCodeDescription"],<|"Message" -> "None"|>],
Failure[ToString[\[FormalX]["StatusCode"]]<>": "<>\[FormalX]["StatusCodeDescription"],<|"Message" -> Lookup[Lookup[ExecuteReponse[\[FormalX]],"error"],"message"]|>]
];


HandleExecuteResponse[\[FormalX]_HTTPResponse]:=If[Quotient[\[FormalX]["StatusCode"],100]===2,
ExecuteReponse[\[FormalX]],
Failure[ToString[\[FormalX]["StatusCode"]]<>": "<>\[FormalX]["StatusCodeDescription"],<|"Message" -> Lookup[Lookup[ExecuteReponse[\[FormalX]],"error"],"message"]|>]
];


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
