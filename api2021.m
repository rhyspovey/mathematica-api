(* ::Package:: *)

(* ::Title:: *)
(*API interfacing*)


(* ::Subtitle:: *)
(*Legacy 2021 version*)


(* ::Text:: *)
(*Rhys G. Povey*)
(*www.rhyspovey.com*)


(* ::Subsection::Closed:: *)
(*Change log*)


(* ::Text:: *)
(*14 November 2021*)
(*- Created SpotifyPlayPlaylist and incorporated SpotifyPlayRandomItem.*)
(**)
(*25 August 2021*)
(*- Added SpotifyPlaylistURIs.*)
(*- Added SpotifyRemoveTracks.*)
(*- Added SpotifyReplaceTrack.*)
(*- Added some error handling to Spotify calls using Enclose and Confirm.*)
(**)
(*06 December 2020*)
(*- Removed URLEncode from APILoginURL.*)
(**)
(*20 August 2020*)
(*- Merged fix of YoutubeItem.*)
(*- Fixed SpotifyAddTracks for single track.*)
(**)
(*6 June 2020*)
(*- Fixed bug with untyped optional argument in YoutubeAddToPlaylist.*)
(**)
(*29 April 2020*)
(*- YoutubeAddToPlaylist can now take optional position argument.*)
(**)
(*7 March 2020*)
(*- Fixed bug in YoutubeUpdatePlaylist and YoutubeCreatePlaylist.*)
(**)
(*2 March 2020*)
(*- Added YoutubeItem.*)
(**)
(*12 February 2020*)
(*- Fixed YoutubePlaylistDetails.*)
(**)
(*5 February 2020*)
(*- APIRead now retries any 5xx error once.*)
(**)
(*29 January 2020*)
(*- Added YoutubePlaylistDetails.*)
(*- Improved YoutubePlaylistItems to handle 403 during.*)
(**)
(*18 January 2020*)
(*- Added playing random item in Spotify playlist.*)
(**)
(*29 November 2019*)
(*- Added Spotify playlist creation and playback control.*)
(**)
(*26 October 2019*)
(*- Changed app detail storage to function APIInfo.*)
(**)
(*4 October 2019*)
(*- Bug fixes in default app handling.*)
(**)
(*27 July 2019*)
(*- First version with Spotify and Youtube login support.*)


(* ::Subsection::Closed:: *)
(*Sample app info*)


(* ::Text:: *)
(*Default app names, others can be used with the option APIInfo->name.*)
(*Omit 'scope' to use unprivileged tokens.*)


(* ::Input:: *)
(*APIInfo["Youtube"]=<|*)
(*"type"->"Youtube",*)
(*"client_id"->"something.apps.googleusercontent.com",*)
(*"client_secret"->"something",*)
(*"redirect_uri"->"urn:ietf:wg:oauth:2.0:oob",*)
(*"scope"->"https://www.googleapis.com/auth/youtube"*)
(*|>;*)


(* ::Input:: *)
(*APIInfo["Spotify"]=<|*)
(*"type"->"Spotify",*)
(*"client_id"->"632something",*)
(*"client_secret"->"something",*)
(*"redirect_uri"->"http://www.redirect.com/page.php",*)
(*"scope"->"playlist-modify-public"*)
(*|>;*)


(* ::Text:: *)
(*Additional app details can be made and called using the name.*)


(* ::Section:: *)
(*Front End*)


BeginPackage["API2021`"];


(* ::Subsection::Closed:: *)
(*Support*)


(* ::Text:: *)
(*Supported APIs:*)


$APIs={"Youtube","Spotify"};


(* ::Subsection::Closed:: *)
(*General*)


ExecuteReponse::usage="ExecuteReponse[HTTPResponse] generates URLExecute output from the HTTPResponse body.";


ReadAuthorizedRequest::usage="ReadAuthorizedRequest[app,HTTPRequest] appends authorization information from app to HTTPRequest and reads.";


(* ::Subsection::Closed:: *)
(*App details*)


APIInfo::usage="APIInfo[app] stores API details with given app name. Functions use option APIInfo\[Rule]app to specify.";


(* ::Subsection::Closed:: *)
(*Login*)


APILoginURL::usage="APILoginURL[app] generates the login URL for specified app.";


APIGetToken::usage="APIGetToken[app,code] using code, obtains an access_token and stores it in app. If no code given, attempts to use refresh_token or obtains an unprivileged token.";


APIGetToken::error="`1` error occured.";


APIGetToken::type="API type `1` not recognized.";


APIGetToken::input="No code given or refesh_token exists.";


APIGetToken::unprivileged="API type `1` does not support unpriviliged tokens.";


APILogin::usage="APILogin[app] provides a dialog for login.";


(* ::Subsection::Closed:: *)
(*Read*)


APIRead::usage="APIRead[app,request] does a URLRead, includes authorization, and refreshes tokens if needed. Returns a HTTPResponse.";


APIRead::error="`1` error occured.";


APIRead::notoken="No access_token exists in app.";


APIRead::refresh="Refreshing token.";


APIRead::gateway="Retrying.";


APIReadResult::usage="APIReadResult[app,request] performs APIRead and returns either Success or Failure.";


APIReadExecute::usage="APIReadResult[app,request,function] performs APIRead and returns function[ExecuteResponse[response]] on success or Failure if not.";


(* ::Subsection:: *)
(*Spotify*)


SpotifyEmptyPlaylist::usage="SpotifyEmptyPlaylist[playlist] empties given playlist.";


SpotifyPlaylistDescription::usage="SpotifyPlaylistDescription[playlist,description] sets playlist description.";


SpotifyAddTracks::usage="SpotifyAddTracks[playlist,tracks,(position)] adds upto 100 tracks (identified by Spotify URI) to playlist. Adds after given position, omit to append.";


SpotifyAddManyTracks::usage="SpotifyAddManyTracks[playlist,tracks] adds more than 100 tracks (identified by Spotify URI) to playlist. Provides progress indicator. Note that Spotify playlist max size is 10,000.";


SpotifySearchReturnURI::usage="SpotifySearchReturnURI[query,type,limit] runs SpotifySearch and returns the Spotify URI.";


SpotifyPlaylistTrackTotal::usage="SpotifyPlaylistTrackTotal[playlist] returns the number of tracks in the playlist.";


SpotifyPlayItem::usage"SpotifyPlayItem[item] begins playback of item. Needs user-modify-playback-state.";


SpotifyPlayPlaylist::usage="SpotifyPlayPlaylist[playlist,track,time] begins playback of playlist given track and time in ms. Track can be integer or \"Random\".";


SpotifyCreatePlaylist::usage="SpotifyCreatePlaylist[user,name,desc,public,collab] creates a playlist for given user with name and desc. public and collab can be true or false lowercase strings.";


SpotifyPlaylistURIs::usage="SpotifyPlaylistURIs[playlist] returns a list of Spotify URIs in playlist.";


SpotifyRemoveTracks::usage="SpotifyRemoveTracks[playlist,tracks] removes upto 100 tracks (identified by Spotify URI) from playlist.";


SpotifyReplaceTrack::usage="SpotifyReplaceTrack[playlist, track out \[Rule] track in] replaces a track in a playlist.";


SpotifySeek::usage="SpotifySeek[time] seeks to time given in ms.";


(* ::Subsubsection:: *)
(*Return HTTPResponse*)


SpotifySearch::usage="SpotifySearch[query,type,limit] searches for query of type, returning upto limit results.";


SpotifyTrack::usage="SpotifyTrack[track] gets info on track (identified by Spotify URI). Gives 200 on success.";


SpotifyPlaylistTrack::usage="SpotifyPlaylistTrack[playlist,index] gets info on track at index in playlist. Gives 200 on success.";


SpotifyAlbum::usage="SpotifyAlbum[album] gets info on album (identified by Spotify URI). Gives 200 on success.";


(* ::Subsection::Closed:: *)
(*Youtube*)


YoutubeTestToken::usage="YoutubeTestToken[] tests token.";


YoutubeSearch::usage="YoutubeSearch[query,type,limit] searches for query of type, returning upto limit results. Gives 200 on success. Costs ~100 quota.";


YoutubeSearchReturnId::usage="YoutubeSearchReturnId[query,type,limit] returns videoids of found videos. Costs ~100 quota.";


YoutubeUpdatePlaylist::usage="YoutubeUpdatePlaylist[playlist,title,description] updates playlist title and description. Gives 200 on success. Costs ~50 quota.";


YoutubePlaylistItems::usage="YoutubePlaylistItems[playlist] returns playlist item ids. Costs ~2 quota per item returned.";


YoutubeDeleteItem::usage="YoutubeDeleteItem[item] deletes playlist item from its playlist. Gives 204 on success. Costs ~50 quota.";


YoutubeDeletePlaylist::usage="YoutubeDeletePlaylist[playlist] deletes playlist. Gives 204 on success. Costs ~50 quota.";


YoutubeCreatePlaylist::usage="YoutubeCreatePlaylist[title,description] creates a playlist. Costs ~50 quota.";


YoutubeAddToPlaylist::usage="YoutubeAddItem[playlist,video] adds video to playlist. Returns playlist resource on success. Costs ~50 quota.";


YoutubePlaylistDetails::usage="YoutubePlaylistDetails[playlist] returns details about specified playlist. Gives 200 on success.";


YoutubeItem::usage="YoutubeItem[item] returns info on an item in a playlist. Costs ~50 quota.";


(* ::Text:: *)
(*With error stopping*)


YoutubeEmptyPlaylist::usage="YoutubeEmptyPlaylist[playlist] systematically deletes every item in playlist. Costs ~52 quota per item deleted.";


YoutubeAddManyToPlaylist::usage="YoutubeAddManyToPlaylist[playlist,videolist]";


(* ::Section:: *)
(*Back end*)


Begin["`Private`"];


(* ::Subsection:: *)
(*General*)


ExecuteReponse[\[FormalX]_HTTPResponse]:=
Module[{string,words,replacements},
string=\[FormalX]["Body"];
words = StringCases[string,RegularExpression["\".*?\""]]; (* avoid doing replacements inside encapsulated strings *)
replacements={":"->"\[Rule]","["->"{","]"->"}"};
ToExpression@StringJoin[If[#[[1]],StringReplace[#[[2]],replacements],#[[2]]]&/@(If[StringQ@#,{True,#},#]&/@StringSplit[string,\[FormalW]:words:>{False,\[FormalW]}])]
];


ReadAuthorizedRequest[app_String,request_HTTPRequest]:=URLRead[Join[request,HTTPRequest[<|"Headers"->{"Authorization"->"Bearer "<>Lookup[APIInfo@app,"access_token"]}|>],2],Interactive->False];


(* ::Subsection::Closed:: *)
(*Login*)


APILoginURL[app_String]:=
Switch[Lookup[APIInfo@app,"type"],

"Youtube",
Hyperlink["Youtube OAuth2 login",URLBuild[<|Method->"GET",
"Scheme"->"https",
"Domain"->"accounts.google.com",
"Path"->"o/oauth2/v2/auth",
"Query"->{
"client_id"->Lookup[APIInfo@app,"client_id"],
"redirect_uri"->"urn:ietf:wg:oauth:2.0:oob",
"response_type"->"code",
"scope"->StringJoin[Riffle[Flatten[{Lookup[APIInfo@app,"scope"]}]," "]],
"access_type"->"offline"
}|>]],

"Spotify",
Hyperlink["Spotify authorization code login",URLBuild[<|Method->"GET",
"Scheme"->"https",
"Domain"->"accounts.spotify.com",
"Path"->"authorize",
"Query"->{
"client_id"->Lookup[APIInfo@app,"client_id"],
"response_type"->"code",
"redirect_uri"->Lookup[APIInfo@app,"redirect_uri"],
"scope"->StringJoin[Riffle[Flatten[{Lookup[APIInfo@app,"scope"]}]," "]]
}|>]],

_,
"API type "<>Lookup[APIInfo@app,"type"]<>" not recognized.";

];


APIGetToken[app_String,code_String:None]:=
Module[{request,read,response},
request=Switch[Lookup[APIInfo@app,"type"],

"Youtube",
HTTPRequest[<|Method->"POST",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"/oauth2/v4/token",
"ContentType"->"application/x-www-form-urlencoded",
"Body"->
Which[StringQ@code,
(* use input code to get new access_token *){
"grant_type"->"authorization_code",
"code"->code,
"client_id"->Lookup[APIInfo@app,"client_id"],
"client_secret"->Lookup[APIInfo@app,"client_secret"],
"redirect_uri"->"urn:ietf:wg:oauth:2.0:oob"
},
KeyExistsQ[APIInfo@app,"refresh_token"],
(* use refresh_token to get a new access_token *){
"grant_type"->"refresh_token",
"refresh_token"->Lookup[APIInfo@app,"refresh_token"],
"client_id"->Lookup[APIInfo@app,"client_id"],
"client_secret"->Lookup[APIInfo@app,"client_secret"],
"redirect_uri"->"urn:ietf:wg:oauth:2.0:oob"
},
Not@KeyExistsQ[APIInfo@app,"scope"],
Message[APIGetToken::unprivileged,Lookup[APIInfo@app,"type"]];Return[$Failed],
True,
Message[APIGetToken::input];Return[$Failed]
]
|>],

"Spotify",
HTTPRequest[<|Method->"POST",
"Scheme"->"https",
"Domain"->"accounts.spotify.com",
"Path"->"api/token",
"ContentType"->"application/x-www-form-urlencoded",
"Headers"->{
"Authorization"->"Basic "<>ExportString[Lookup[APIInfo@app,"client_id"]<>":"<>Lookup[APIInfo@app,"client_secret"],"Base64"]
},
"Body"->
Which[StringQ@code,
 (* use input code to get new access_token*) {
"grant_type"->"authorization_code",
"code"->code,
"redirect_uri"->Lookup[APIInfo@app,"redirect_uri"]
},
KeyExistsQ[APIInfo@app,"refresh_token"],
(* use refresh_token to get a new access_token *){
"grant_type"->"refresh_token",
"refresh_token"->Lookup[APIInfo@app,"refresh_token"]
},
Not@KeyExistsQ[APIInfo@app,"scope"],
 (* client token with no privileges *) {
"grant_type"->"client_credentials"
},
True,
Message[APIGetToken::input];Return[$Failed]
]
|>],
_,
Message[APIGetToken::type,Lookup[APIInfo@app,"type"]];Return[$Failed]
];

read=URLRead[request,Interactive->False];
response=ExecuteReponse@read;

If[read["StatusCode"]==200, (* check if successful *)
AppendTo[APIInfo@app,DeleteMissing[(Association@@response)[[{"access_token","refresh_token"}]]]],
Message[APIGetToken::error,read["StatusCode"]]
];

(* output *)
response
];


APILogin[app_String]:=
DynamicModule[{logincode},
logincode=Null;
DialogInput@DialogNotebook[{
APILoginURL[app],
InputField[Dynamic@logincode,String,FieldHint->"Access code",FieldSize->{50,2}],
DefaultButton[DialogReturn[APIGetToken[app,logincode]]]
}]
];


(* ::Subsection:: *)
(*Read*)


APIRead[app_String,request_HTTPRequest]:=Module[{read},
(* initial check token exists *)
If[Not@KeyExistsQ[APIInfo@app,"access_token"],Message[APIRead::notoken]];

(* read url request *)
read=ReadAuthorizedRequest[app,request];

Which[
Quotient[read["StatusCode"],100]==2, (* success *)
read,
read["StatusCode"]==401, (* refresh token, try again *)
Message[APIRead::refresh];
APIGetToken[app];Pause[2];read=ReadAuthorizedRequest[app,request],
Quotient[read["StatusCode"],100]==5, (* bad gateway error, try again once *)
Message[APIRead::gateway];
Pause[10];read=ReadAuthorizedRequest[app,request],
True, (* other error *)
Message[APIRead::error,read["StatusCode"]];read
]

];


APIReadResult[app_String,request_HTTPRequest,message_String:"None"]:=Module[{read,code,desc},
(* do APIRead *)
read=APIRead[app,request];
code=read["StatusCode"];
desc=read["StatusCodeDescription"];
If[Quotient[code,100]==2,
Success[ToString[code]<>": "<>desc,<|"Message" -> message|>],
Failure[ToString[code]<>": "<>desc,<|"Message" -> Lookup[Lookup[ExecuteReponse[read],"error"],"message"]|>]]
];


APIReadExecute[app_String,request_HTTPRequest,func_:Identity]:=Module[{read,code,desc},
(* do APIRead *)
read=APIRead[app,request];
code=read["StatusCode"];
desc=read["StatusCodeDescription"];
If[Quotient[code,100]==2,
func[ExecuteReponse[read]],
Failure[ToString[code]<>": "<>desc,<|"Message" -> Lookup[Lookup[ExecuteReponse[read],"error"],"message"]|>]]
];


(* ::Subsection:: *)
(*Spotify*)


SpotifyEmptyPlaylist[playlist_String,OptionsPattern[]]:=
APIReadResult[OptionValue[APIInfo],
HTTPRequest[<|Method->"PUT",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist<>"/tracks",
"ContentType"->"application/json",
"Query"->{"uris"->""}
|>],"Playlist emptied"];


Options[SpotifyEmptyPlaylist]={APIInfo->"Spotify"};


SpotifyPlaylistDescription[playlist_String,description_String,OptionsPattern[]]:=
APIReadResult[OptionValue[APIInfo],
HTTPRequest[<|Method->"PUT",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist,
"ContentType"->"application/json",
"Body"->ExportString[{"description"->description},"JSON","Compact"->True]
|>],"Playlist description updated"];


Options[SpotifyPlaylistDescription]={APIInfo->"Spotify"};


SpotifyAddTracks[playlist_String,tracks_List/;Length[tracks]<=100,position_Integer:-1,OptionsPattern[]]:=
APIReadResult[OptionValue[APIInfo],
HTTPRequest[<|Method->"POST",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist<>"/tracks",
"ContentType"->"application/json",
"Body"->ExportString[{"uris"->tracks,If[NonNegative[position],"position"->ToString[position],Nothing]},"JSON","Compact"->True]
|>],ToString[Length[tracks]]<>" tracks added"];


SpotifyAddTracks[playlist_String,track_String,options:OptionsPattern[]]:=SpotifyAddTracks[playlist,{track},options];


Options[SpotifyAddTracks]={APIInfo->"Spotify"};


SpotifyAddManyTracks[playlist_String,tracks_List,options:OptionsPattern[]]:=
Module[{trackblocks,n,i,limit=100},
trackblocks=Partition[tracks,limit,limit,{1,1},{}];

n=Length[trackblocks];
i=0;
PrintTemporary[Row[{ProgressIndicator[Dynamic[i/n]]," ",Dynamic[i],"/",n}]];

Enclose[
Scan[(
	Confirm[SpotifyAddTracks[playlist,#,FilterRules[{options},Options[SpotifyAddTracks]]],"Completed "<>ToString[i]<>"\[Times]"<>ToString[limit]<>" tracks"];
	i++;Pause[OptionValue@Pause]
	)&,trackblocks];
Success["AddedTracks", <|"MessageTemplate" -> "`Number` tracks added","MessageParameters"-> <|"Number" -> Length[tracks]|>|>]
]
];


Options[SpotifyAddManyTracks]={Pause->1,APIInfo->"Spotify"};


SpotifySearch[search_String,type_String:"track",limit_Integer:1,OptionsPattern[]]:=
APIRead[OptionValue[APIInfo],HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/search",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"type"->type,"q"->search,"limit"->ToString[limit]}
|>]];


Options[SpotifySearch]={APIInfo->"Spotify"};


SpotifySearchReturnURI[search_String,type_String:"track",limit_Integer:1,OptionsPattern[]]:=
Lookup["uri"]/@Lookup[Lookup[ExecuteReponse[SpotifySearch[search,type,limit,APIInfo->OptionValue[APIInfo]]],type<>"s"],"items"];


Options[SpotifySearchReturnURI]={APIInfo->"Spotify"};


SpotifyTrack[trackid_String,OptionsPattern[]]:=
APIRead[OptionValue[APIInfo],HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/tracks/"<>trackid,
"ContentType"->"application/x-www-form-urlencoded"
|>]];


Options[SpotifyTrack]={APIInfo->"Spotify"};


SpotifyPlaylistTrack[playlist_String,index_Integer,OptionsPattern[]]:=
APIRead[OptionValue[APIInfo],
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist<>"/tracks",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"offset"->ToString[index-1],"limit"->"1","fields"->"items"}
|>]];


Options[SpotifyPlaylistTrack]={APIInfo->"Spotify"};


SpotifyAlbum[album_String,OptionsPattern[]]:=
APIRead[OptionValue[APIInfo],
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/albums/"<>album,
"ContentType"->"application/x-www-form-urlencoded"
|>]];


Options[SpotifyAlbum]={APIInfo->"Spotify"};


SpotifyPlaylistTrackTotal[playlist_String,OptionsPattern[]]:=
APIReadExecute[OptionValue[APIInfo],
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist<>"/tracks",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"fields"->"total"}
|>],
Lookup[#,"total"]&];


Options[SpotifyPlaylistTrackTotal]={APIInfo->"Spotify"};


SpotifyPlayItem[play_String,OptionsPattern[]]:=
APIReadResult[OptionValue[APIInfo],
HTTPRequest[<|Method->"PUT",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"/v1/me/player/play",
"ContentType"->"application/json",
"Body"->ExportString[{"context_uri"->play},"JSON","Compact"->True]
|>]];


Options[SpotifyPlayItem]={APIInfo->"Spotify"};


SpotifyPlayPlaylist[playlist_String,track_:"Random",time_Integer:0,options:OptionsPattern[]]:=Module[{tracks,position},
position=If[IntegerQ[track],
	track,
	tracks=SpotifyPlaylistTrackTotal[playlist,options];
	RandomInteger[{0,tracks-1}]
];
APIReadResult[OptionValue[APIInfo],
HTTPRequest[<|Method->"PUT",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"/v1/me/player/play",
"ContentType"->"application/json",
"Body"->ExportString[{"context_uri"->"spotify:playlist:"<>playlist,"offset"->{"position"->position},"position_ms"->time},"JSON","Compact"->True]
|>]]
];


Options[SpotifyPlayPlaylist]={APIInfo->"Spotify"};


SpotifyCreatePlaylist[user_String,name_String,desc_String:"",public_String:"false",collab_String:"false",OptionsPattern[]]:=
APIReadResult[OptionValue[APIInfo],
HTTPRequest[<|Method->"POST",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"/v1/users/"<>user<>"/playlists",
"ContentType"->"application/json",
"Body"->ExportString[{"name"->name,"description"->desc,"public"->public,"collaborative"->collab},"JSON","Compact"->True]
|>],"Playlist created"];


Options[SpotifyCreatePlaylist]={APIInfo->"Spotify"};


SpotifyPlaylistURIs[playlist_String,options:OptionsPattern[]]:=Module[{limit=100,total},
Enclose[
total=Confirm[SpotifyPlaylistTrackTotal[playlist,options]];
Join@@Table[
Lookup[Lookup[#,"track"],"uri"]&/@Confirm[APIReadExecute["Spotify",
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist<>"/tracks",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"offset"->ToString[offset],"limit"->ToString[limit],"fields"->"items(track(uri))"}
|>],Lookup[#,"items"]&]],
{offset,Range[0,total,limit]}]
]
];


Options[SpotifyPlaylistURIs]={APIInfo->"Spotify"};


SpotifyRemoveTracks[playlist_String,tracks_List/;Length[tracks]<=100,OptionsPattern[]]:=
APIReadResult[OptionValue[APIInfo],
HTTPRequest[<|Method->"DELETE",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist<>"/tracks",
"ContentType"->"application/json",
"Body"->ExportString[{"uris"->tracks},"JSON","Compact"->True]
|>],ToString[Length[tracks]]<>" tracks removed"];


Options[SpotifyRemoveTracks]={APIInfo->"Spotify"};


SpotifyReplaceTrack[playlist_String,trackout_String->trackin_String,options:OptionsPattern[]]:=Module[{positions,pos},
Enclose[
positions=Position[Confirm[SpotifyPlaylistURIs[playlist,options]],trackout];
If[Length[positions]==1,
	pos=positions[[1,1]];
	Confirm[SpotifyRemoveTracks[playlist,{trackout},options]];
	Confirm[SpotifyAddTracks[playlist,{trackin},pos-1,options]];
	Success["ReplacedTrack",<|"Message"->"Track replaced"|>],
	Failure["ReplacedTrack",<|"MessageTemplate"->"Track appears `Number` times in playlist","MessageParameters"-> <|"Number"->Length[positions]|>|>]
]
]
];


Options[SpotifyReplaceTrack]={APIInfo->"Spotify"};


SpotifySeek[time_Integer,OptionsPattern[]]:=
APIReadResult[OptionValue[APIInfo],
HTTPRequest[<|Method->"PUT",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"/v1/me/player/seek",
"ContentType"->"application/json",
{"position_ms"->ToString[time]}
|>]];


Options[SpotifySeek]={APIInfo->"Spotify"};


(* ::Subsection::Closed:: *)
(*Youtube*)


YoutubeSearch[search_String,type_String:"video",limit_Integer:5,return_String:"snippet",OptionsPattern[]]:=
APIRead[OptionValue[APIInfo],HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/search",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"part"->return,"type"->type,"q"->search,"maxResults"->ToString[limit]}
|>]];


Options[YoutubeSearch]={APIInfo->"Youtube"};


YoutubeSearchReturnId[search_String,type_String:"video",limit_Integer:1,OptionsPattern[]]:=
Lookup["videoId"]/@Lookup[Lookup[ExecuteReponse[YoutubeSearch[search,type,limit,"id",APIInfo->OptionValue[APIInfo]]],"items"],"id"];


Options[YoutubeSearchReturnId]={APIInfo->"Youtube"};


YoutubeUpdatePlaylist[playlistid_String,title_String,description_String,OptionsPattern[]]:=
APIRead[OptionValue[APIInfo],
HTTPRequest[<|Method->"PUT",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlists",
"ContentType"->"application/json",
"Query"->{"part"->"snippet"},
"Body"->ExportString[{
"id"->playlistid,
"snippet"->{"title"->title,"description"->description}
},"JSON","Compact"->True]
|>]];


Options[YoutubeUpdatePlaylist]={APIInfo->"Youtube"};


YoutubePlaylistItems[playlistid_String,OptionsPattern[]]:=
Module[{response,next,proceed},

Flatten[Reap[

response=ExecuteReponse[APIRead[OptionValue[APIInfo],
HTTPRequest[<|Method->"GET","Scheme"->"https","Domain"->"www.googleapis.com","Path"->"youtube/v3/playlistItems","ContentType"->"application/x-www-form-urlencoded",
"Query"->{
"part"->"id",
"playlistId"->playlistid,
"maxResults"->"50"
}
|>]]];
Sow[Lookup[#,"id"]&/@Lookup[response,"items"]];

While[KeyExistsQ[response,"nextPageToken"],
next=Lookup[response,"nextPageToken"];
response=ExecuteReponse[APIRead[OptionValue[APIInfo],
HTTPRequest[<|Method->"GET","Scheme"->"https","Domain"->"www.googleapis.com","Path"->"youtube/v3/playlistItems","ContentType"->"application/x-www-form-urlencoded",
"Query"->{
"part"->"id",
"playlistId"->playlistid,
"maxResults"->"50",
"pageToken"->next
}
|>]]];
Sow[Lookup[#,"id"]&/@Lookup[response,"items"]];

];

][[2,1]]]

];


Options[YoutubePlaylistItems]={APIInfo->"Youtube"};


YoutubeItem[itemid_String,OptionsPattern[]]:=
APIRead[OptionValue[APIInfo],
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlistItems",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"part"->"snippet","id"->itemid}
|>]]


Options[YoutubeItem]={APIInfo->"Youtube"};


YoutubeDeleteItem[itemid_String,OptionsPattern[]]:=
APIRead[OptionValue[APIInfo],
HTTPRequest[<|Method->"DELETE",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlistItems",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"id"->itemid}
|>]];


Options[YoutubeDeleteItem]={APIInfo->"Youtube"};


YoutubeDeletePlaylist[playlistid_String,OptionsPattern[]]:=
APIRead[OptionValue[APIInfo],
HTTPRequest[<|Method->"DELETE",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlists",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"id"->playlistid}
|>]];


Options[YoutubeDeletePlaylist]={APIInfo->"Youtube"};


YoutubeCreatePlaylist[title_String,description_String:"",OptionsPattern[]]:=
Module[{response},

response=APIRead[OptionValue[APIInfo],
HTTPRequest[<|Method->"POST",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlists",
"ContentType"->"application/json",
"Query"->{"part"->"snippet"},
"Body"->ExportString[{
"snippet"->{"title"->title,"description"->description}
},"JSON","Compact"->True]
|>]];

If[response["StatusCode"]==200,Lookup[ExecuteReponse@response,"id"],$Failed]

];


Options[YoutubeCreatePlaylist]={APIInfo->"Youtube"};


YoutubeAddToPlaylist[playlistid_String,videoid_String,position_Integer:Missing[],OptionsPattern[]]:=
APIRead[OptionValue[APIInfo],
HTTPRequest[<|Method->"POST",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlistItems",
"ContentType"->"application/json",
"Query"->{"part"->"snippet"},
"Body"->ExportString[{
"snippet"->{
"resourceId"->{"kind"->"youtube#video","videoId"->videoid},
"playlistId"->playlistid
}~Join~If[Not@MissingQ@position,{"position"->ToString[position]},{}]
},"JSON","Compact"->True]
|>]];


Options[YoutubeAddToPlaylist]={APIInfo->"Youtube"};


YoutubePlaylistDetails[playlistid_String,OptionsPattern[]]:=
APIRead[OptionValue[APIInfo],
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlists",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{
"part"->"contentDetails",
"id"->playlistid
}
|>]];


Options[YoutubePlaylistDetails]={APIInfo->"Youtube"};


(* ::Subsubsection:: *)
(*Runs into quota limit*)


(* ::Text:: *)
(*Stops on first failure*)


YoutubeEmptyPlaylist[id_String,OptionsPattern[]]:=
Module[{items,pause,n,i,code,read,proceed},
items=YoutubePlaylistItems[id,APIInfo->OptionValue[APIInfo]];

n=Length[items];
i=0;
Print[Row[{ProgressIndicator[Dynamic[i/n]]," ",Dynamic[i],"/",n}]];

proceed=(n>0);
While[proceed,
Pause[OptionValue@Pause];
read=YoutubeDeleteItem[items[[i+1]],APIInfo->OptionValue[APIInfo]];
code=read["StatusCode"];

If[Quotient[code,100]==2,i++,proceed=False];
If[i==n,proceed=False];
];

(* output remaining number of items *)
n-i

];


Options[YoutubeEmptyPlaylist]={Pause->0.1,APIInfo->"Youtube"};


YoutubeAddManyToPlaylist[playlistid_String,videoidlist_List,OptionsPattern[]]:=
Module[{n,i,code,read,proceed},

n=Length[videoidlist];
i=0;
Print[Row[{ProgressIndicator[Dynamic[i/n]]," ",Dynamic[i],"/",n}]];

proceed=(n>0);
While[proceed,
Pause[OptionValue@Pause];
read=YoutubeAddToPlaylist[playlistid,videoidlist[[i+1]],APIInfo->OptionValue[APIInfo]];
code=read["StatusCode"];

If[Quotient[code,100]==2,i++,proceed=False];
If[i==n,proceed=False];
];

(* output remaining items to be added *)
videoidlist[[i+1;;]]

];


Options[YoutubeAddManyToPlaylist]={Pause->1,APIInfo->"Youtube"};


YoutubePlaylistItems[playlistid_String,OptionsPattern[]]:=
Module[{response,next,proceed,list,read},

list=Flatten[Reap[

read=APIRead[OptionValue[APIInfo],
HTTPRequest[<|Method->"GET","Scheme"->"https","Domain"->"www.googleapis.com","Path"->"youtube/v3/playlistItems","ContentType"->"application/x-www-form-urlencoded",
"Query"->{
"part"->"id",
"playlistId"->playlistid,
"maxResults"->"50"
}
|>]];
proceed=(Quotient[read["StatusCode"],100]==2);
If[proceed,
response=ExecuteReponse[read];
Sow[Lookup[#,"id"]&/@Lookup[response,"items"]];
];

While[proceed \[And] KeyExistsQ[response,"nextPageToken"],
next=Lookup[response,"nextPageToken"];
read=APIRead[OptionValue[APIInfo],
HTTPRequest[<|Method->"GET","Scheme"->"https","Domain"->"www.googleapis.com","Path"->"youtube/v3/playlistItems","ContentType"->"application/x-www-form-urlencoded",
"Query"->{
"part"->"id",
"playlistId"->playlistid,
"maxResults"->"50",
"pageToken"->next
}
|>]];
proceed=(Quotient[read["StatusCode"],100]==2);
If[proceed,
response=ExecuteReponse[read];
Sow[Lookup[#,"id"]&/@Lookup[response,"items"]];
];

];

][[2,1]]];

(* output *)
If[proceed\[Or]Not[OptionValue["FailOnIncomplete"]],
list,
$Failed
]

];


Options[YoutubePlaylistItems]={APIInfo->"Youtube","FailOnIncomplete"->True};


(* ::Subsubsection:: *)
(*Diagnostic*)


YoutubeTestToken[OptionsPattern[]]:=ReadAuthorizedRequest[OptionValue[APIInfo],
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"oauth2/v3/tokeninfo",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"access_token"->Lookup[APIInfo[OptionValue[APIInfo]],"access_token"]}
|>]
];


Options[YoutubeTestToken]={APIInfo->"Youtube"};


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
