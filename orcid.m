(* ::Package:: *)

(* ::Title:: *)
(*ORCID API*)


(* ::Text:: *)
(*Rhys G. Povey*)
(*www.rhyspovey.com*)


(* ::Section:: *)
(*Front End*)


BeginPackage["ORCID`",{"API`"}];


(* ::Subsection:: *)
(*Login*)


LoginURL;


TokenRequest;


(* ::Subsection:: *)
(*Calls*)


ORCIDPublic::usage="ORCIDPublic[orcid(string),endpoint(string)] retrieves data in JSON format and formats it. Available endpoints given at https://info.orcid.org/documentation/api-tutorials/api-tutorial-read-data-on-a-record/.";


DOI::usage="DOI[doi] calls and returns DOI info.";


(* ::Section:: *)
(*Back end*)


Begin["`Private`"];


(* ::Subsection::Closed:: *)
(*Login*)


(* ::Text:: *)
(*Only supporting public reading ("scope"->"/read-public").*)


TokenRequest["ORCID",appdetails_Association]:=
Enclose[
HTTPRequest[<|
	Method->"POST",
	"Scheme"->"https",
	"Domain"->"orcid.org",
	"Path"->"oauth/token",
	"ContentType"->"application/x-www-form-urlencoded",
	"Body"->{
		"grant_type"->"client_credentials",
		"client_id"->Confirm@Lookup[appdetails,"client_id"],
		"client_secret"->Confirm@Lookup[appdetails,"client_secret"],
		"scope"->Confirm@Lookup[appdetails,"scope"]
	}
|>]
];


(* ::Subsection:: *)
(*Calls*)


$defaultorcid={API->"ORCID","Version"->"v3.0"};


ORCIDPublic[orcid_String,endpoint_String,OptionsPattern[$defaultorcid]]:=
APIReadExecute[OptionValue[API],
HTTPRequest[<|Method->"PUT",
"Scheme"->"https",
"Domain"->"pub.orcid.org",
"Path"->StringJoin[Riffle[{OptionValue["Version"],orcid,endpoint},"/"]],
"ContentType"->"application/orcid+json"
|>],"Playlist description updated"];


DOI[doi_String]:=Module[{read,code,desc},
read=URLRead@HTTPRequest[<|
	Method->"GET",
	"Scheme"->"https",
	"Domain"->"doi.org",
	"Path"->"api/handles/"<>doi,
	"ContentType"->"application/json"
|>];
code=read["StatusCode"];
desc=read["StatusCodeDescription"];
If[Quotient[code,100]==2,
ExecuteReponse[read],
Failure[ToString[code]<>": "<>desc,<|"Message" -> Lookup[Lookup[ExecuteReponse[read],"error"],"message"]|>]]
]


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
