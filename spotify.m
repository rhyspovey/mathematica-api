(* ::Package:: *)

(* ::Title:: *)
(*Spotify API*)


(* ::Text:: *)
(*Rhys G. Povey*)
(*www.rhyspovey.com*)


(* ::Section:: *)
(*Front End*)


BeginPackage["Spotify`",{"API`"}];


(* ::Subsection:: *)
(*Login*)


LoginURL;


TokenRequest;


(* ::Subsection:: *)
(*Calls*)


SpotifyEmptyPlaylist::usage="SpotifyEmptyPlaylist[playlist] empties given playlist.";


SpotifyPlaylistDescription::usage="SpotifyPlaylistDescription[playlist,description] sets playlist description.";


SpotifyAddTracks::usage="SpotifyAddTracks[playlist,tracks,(position)] adds upto 100 tracks (identified by Spotify URI) to playlist. Adds after given position, omit to append.";


SpotifyAddManyTracks::usage="SpotifyAddManyTracks[playlist,tracks] adds more than 100 tracks (identified by Spotify URI) to playlist. Provides progress indicator. Note that Spotify playlist max size is 10,000.";


SpotifySearchReturnURI::usage="SpotifySearchReturnURI[query,type,limit] runs SpotifySearch and returns the Spotify URI.";


SpotifyPlaylistTrackTotal::usage="SpotifyPlaylistTrackTotal[playlist] returns the number of tracks in the playlist.";


SpotifyPlayItem::usage"SpotifyPlayItem[item] begins playback of item. Needs user-modify-playback-state.";


SpotifyPlayPlaylist::usage="SpotifyPlayPlaylist[playlist,track,time] begins playback of playlist given track and time in ms. Track can be integer or \"Random\".";


SpotifyCreatePlaylist::usage="SpotifyCreatePlaylist[user,name,desc,public,collab] creates a playlist for given user with name and desc. public and collab can be true or false lowercase strings.";


SpotifyPlaylistURIs::usage="SpotifyPlaylistURIs[playlist] returns a list of Spotify URIs in playlist.";


SpotifyRemoveTracks::usage="SpotifyRemoveTracks[playlist,tracks] removes upto 100 tracks (identified by Spotify URI) from playlist.";


SpotifyReplaceTrack::usage="SpotifyReplaceTrack[playlist, track out \[Rule] track in] replaces a track in a playlist.";


SpotifySeek::usage="SpotifySeek[time] seeks to time given in ms.";


(* ::Subsubsection:: *)
(*Return HTTPResponse*)


SpotifySearch::usage="SpotifySearch[query,type,limit] searches for query of type, returning upto limit results.";


SpotifyTrack::usage="SpotifyTrack[track] gets info on track (identified by Spotify URI). Gives 200 on success.";


SpotifyPlaylistTrack::usage="SpotifyPlaylistTrack[playlist,index] gets info on track at index in playlist. Gives 200 on success.";


SpotifyAlbum::usage="SpotifyAlbum[album] gets info on album (identified by Spotify URI). Gives 200 on success.";


SpotifyPlaylist::usage="SpotifyPlaylist[playlist] gets info on playlist (identified by Spotify URI). Gives 200 on success.";


(* ::Section:: *)
(*Back end*)


Begin["`Private`"];


APIQ["Spotify"]=True;


(* ::Subsection:: *)
(*Login*)


LoginURL["Spotify",appdetails_Association]:=
Enclose[
Hyperlink["Spotify authorization code login",URLBuild[<|
	Method->"GET",
	"Scheme"->"https",
	"Domain"->"accounts.spotify.com",
	"Path"->"authorize",
	"Query"->{
		"client_id"->Confirm@Lookup[appdetails,"client_id"],
		"response_type"->"code",
		"redirect_uri"->Confirm@Lookup[appdetails,"redirect_uri"],
		If[KeyMemberQ[appdetails,"scope"],
			"scope"->StringJoin[Riffle[Flatten[{Lookup[appdetails,"scope"]}]," "]],
			Message[LoginURL::scope];Nothing
		]
	}
|>]]
];


TokenRequest["Spotify",appdetails_Association,code_:None]:=
Enclose[
HTTPRequest[<|
	Method->"POST",
	"Scheme"->"https",
	"Domain"->"accounts.spotify.com",
	"Path"->"api/token",
	"ContentType"->"application/x-www-form-urlencoded",
	"Headers"->{
		"Authorization"->"Basic "<>ExportString[Confirm@Lookup[appdetails,"client_id"]<>":"<>Confirm@Lookup[appdetails,"client_secret"],"Base64"]
	},
	"Body"->Which[
	
	StringQ@code, (* use input code to get new access_token*) {
		"grant_type"->"authorization_code",
		"code"->code,
		"redirect_uri"->Confirm@Lookup[appdetails,"redirect_uri"]
	},
	
	KeyExistsQ[appdetails,"refresh_token"], (* use refresh_token to get a new access_token *){
		"grant_type"->"refresh_token",
		"refresh_token"->Lookup[appdetails,"refresh_token"]
	},
	
	Not@KeyExistsQ[appdetails,"scope"], (* client token with no privileges *) {
		"grant_type"->"client_credentials"
	},
	
	True,
	Confirm[Failure["Input",<|"Message"->"No code or refresh_token given for privileged access."|>]]
	]
|>]
];


(* ::Subsection:: *)
(*Calls*)


$defaultspotify={API->"Spotify"};


SpotifyEmptyPlaylist[playlist_String,OptionsPattern[$defaultspotify]]:=
APIReadResult[OptionValue[API],
HTTPRequest[<|Method->"PUT",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist<>"/tracks",
"ContentType"->"application/json",
"Query"->{"uris"->""}
|>],"Playlist emptied"];


SpotifyPlaylistDescription[playlist_String,description_String,OptionsPattern[$defaultspotify]]:=
APIReadResult[OptionValue[API],
HTTPRequest[<|Method->"PUT",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist,
"ContentType"->"application/json",
"Body"->ExportString[{"description"->description},"JSON","Compact"->True]
|>],"Playlist description updated"];


SpotifyAddTracks[playlist_String,tracks_List/;Length[tracks]<=100,position_Integer:-1,OptionsPattern[$defaultspotify]]:=
APIReadResult[OptionValue[API],
HTTPRequest[<|Method->"POST",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist<>"/tracks",
"ContentType"->"application/json",
"Body"->ExportString[{"uris"->tracks,If[NonNegative[position],"position"->ToString[position],Nothing]},"JSON","Compact"->True]
|>],ToString[Length[tracks]]<>" tracks added"];


SpotifyAddTracks[playlist_String,track_String,opts:OptionsPattern[$defaultspotify]]:=SpotifyAddTracks[playlist,{track},opts];


SpotifyAddManyTracks[playlist_String,tracks_List,opts:OptionsPattern[$defaultspotify]]:=
Module[{trackblocks,n,i,limit=100,pause=1},
trackblocks=Partition[tracks,limit,limit,{1,1},{}];

n=Length[trackblocks];
i=0;
PrintTemporary[Row[{ProgressIndicator[Dynamic[i/n]]," ",Dynamic[i],"/",n}]];

Enclose[
Scan[(
	Confirm[SpotifyAddTracks[playlist,#,FilterRules[{opts},Options[SpotifyAddTracks]]],"Completed "<>ToString[i]<>"\[Times]"<>ToString[limit]<>" tracks"];
	i++;Pause[pause]
	)&,trackblocks];
Success["AddedTracks", <|"MessageTemplate" -> "`Number` tracks added","MessageParameters"-> <|"Number" -> Length[tracks]|>|>]
]
];


SpotifySearch[search_String,type_String:"track",limit_Integer:1,OptionsPattern[$defaultspotify]]:=
APIRead[OptionValue[API],HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/search",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"type"->type,"q"->search,"limit"->ToString[limit]}
|>]];


SpotifySearchReturnURI[search_String,type_String:"track",limit_Integer:1,opts:OptionsPattern[$defaultspotify]]:=
Lookup["uri"]/@Lookup[Lookup[ExecuteReponse[SpotifySearch[search,type,limit,opts]],type<>"s"],"items"];


SpotifyTrack[trackid_String,OptionsPattern[$defaultspotify]]:=
APIRead[OptionValue[API],HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/tracks/"<>trackid,
"ContentType"->"application/x-www-form-urlencoded"
|>]];


SpotifyPlaylistTrack[playlist_String,index_Integer,OptionsPattern[$defaultspotify]]:=
APIRead[OptionValue[API],
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist<>"/tracks",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"offset"->ToString[index-1],"limit"->"1","fields"->"items"}
|>]];


SpotifyAlbum[album_String,OptionsPattern[$defaultspotify]]:=
APIRead[OptionValue[API],
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/albums/"<>album,
"ContentType"->"application/x-www-form-urlencoded"
|>]];


SpotifyPlaylist[playlist_String,OptionsPattern[$defaultspotify]]:=
APIRead[OptionValue[API],HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist,
"ContentType"->"application/x-www-form-urlencoded"
|>]];


SpotifyPlaylistTrackTotal[playlist_String,OptionsPattern[$defaultspotify]]:=
APIReadExecute[OptionValue[API],
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist<>"/tracks",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"fields"->"total"}
|>],
Lookup[#,"total"]&];


SpotifyPlayItem[play_String,OptionsPattern[$defaultspotify]]:=
APIReadResult[OptionValue[API],
HTTPRequest[<|Method->"PUT",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"/v1/me/player/play",
"ContentType"->"application/json",
"Body"->ExportString[{"context_uri"->play},"JSON","Compact"->True]
|>]];


SpotifyPlayPlaylist[playlist_String,track:_Integer|_String:"Random",time_Integer:0,opts:OptionsPattern[$defaultspotify]]:=Module[{tracks,position},
position=If[IntegerQ[track],
	track,
	tracks=SpotifyPlaylistTrackTotal[playlist,opts];
	RandomInteger[{0,tracks-1}]
];
APIReadResult[OptionValue[API],
HTTPRequest[<|Method->"PUT",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"/v1/me/player/play",
"ContentType"->"application/json",
"Body"->ExportString[{"context_uri"->"spotify:playlist:"<>playlist,"offset"->{"position"->position},"position_ms"->time},"JSON","Compact"->True]
|>]]
];


SpotifyCreatePlaylist[user_String,name_String,desc_String:"",public_String:"false",collab_String:"false",OptionsPattern[$defaultspotify]]:=
APIReadResult[OptionValue[API],
HTTPRequest[<|Method->"POST",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"/v1/users/"<>user<>"/playlists",
"ContentType"->"application/json",
"Body"->ExportString[{"name"->name,"description"->desc,"public"->public,"collaborative"->collab},"JSON","Compact"->True]
|>],"Playlist created"];


SpotifyPlaylistURIs[playlist_String,opts:OptionsPattern[$defaultspotify]]:=Module[{limit=100,total},
Enclose[
total=Confirm[SpotifyPlaylistTrackTotal[playlist,opts]];
Join@@Table[
Lookup[Lookup[#,"track"],"uri"]&/@Confirm[APIReadExecute["Spotify",
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist<>"/tracks",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"offset"->ToString[offset],"limit"->ToString[limit],"fields"->"items(track(uri))"}
|>],Lookup[#,"items"]&]],
{offset,Range[0,total,limit]}]
]
];


SpotifyRemoveTracks[playlist_String,tracks_List/;Length[tracks]<=100,OptionsPattern[$defaultspotify]]:=
APIReadResult[OptionValue[API],
HTTPRequest[<|Method->"DELETE",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"v1/playlists/"<>playlist<>"/tracks",
"ContentType"->"application/json",
(*OLD "Body"->ExportString[{"uris"->tracks},"JSON","Compact"->True] *)
"Body"->ExportString[{"tracks"->({"uri"->#}&/@tracks)},"JSON","Compact"->True]
|>],ToString[Length[tracks]]<>" tracks removed"];


SpotifyReplaceTrack[playlist_String,trackout_String->trackin_String,opts:OptionsPattern[$defaultspotify]]:=Module[{positions,pos},
Enclose[
positions=Position[Confirm[SpotifyPlaylistURIs[playlist,opts]],trackout];
If[Length[positions]==1,
	pos=positions[[1,1]];
	Confirm[SpotifyRemoveTracks[playlist,{trackout},opts]];
	Confirm[SpotifyAddTracks[playlist,{trackin},pos-1,opts]];
	Success["ReplacedTrack",<|"Message"->"Track replaced"|>],
	Failure["ReplacedTrack",<|"MessageTemplate"->"Track appears `Number` times in playlist","MessageParameters"-> <|"Number"->Length[positions]|>|>]
]
]
];


SpotifySeek[time_Integer,OptionsPattern[$defaultspotify]]:=
APIReadResult[OptionValue[API],
HTTPRequest[<|Method->"PUT",
"Scheme"->"https",
"Domain"->"api.spotify.com",
"Path"->"/v1/me/player/seek",
"ContentType"->"application/json",
{"position_ms"->ToString[time]}
|>]];


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
