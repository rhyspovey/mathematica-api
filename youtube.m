(* ::Package:: *)

(* ::Title:: *)
(*YouTube API*)


(* ::Text:: *)
(*Rhys G. Povey*)
(*www.rhyspovey.com*)


(* ::Section:: *)
(*Front End*)


BeginPackage["YouTube`",{"API`"}];


(* ::Subsection:: *)
(*Login*)


LoginURL;


TokenRequest;


(* ::Subsection:: *)
(*Calls*)


YouTubeTestToken::usage="YoutubeTestToken[] tests token.";


YouTubeSearch::usage="YoutubeSearch[query,type,limit] searches for query of type, returning upto limit results. Gives 200 on success. Costs ~100 quota.";


YouTubeSearchReturnId::usage="YoutubeSearchReturnId[query,type,limit] returns videoids of found videos. Costs ~100 quota.";


YouTubeUpdatePlaylist::usage="YoutubeUpdatePlaylist[playlist,title,description] updates playlist title and description. Gives 200 on success. Costs ~50 quota.";


YouTubePlaylistItems::usage="YoutubePlaylistItems[playlist] returns playlist item ids. Costs ~2 quota per item returned.";


YouTubeDeleteItem::usage="YoutubeDeleteItem[item] deletes playlist item from its playlist. Gives 204 on success. Costs ~50 quota.";


YouTubeDeletePlaylist::usage="YoutubeDeletePlaylist[playlist] deletes playlist. Gives 204 on success. Costs ~50 quota.";


YouTubeCreatePlaylist::usage="YoutubeCreatePlaylist[title,description] creates a playlist. Costs ~50 quota.";


YouTubeAddToPlaylist::usage="YoutubeAddItem[playlist,video] adds video to playlist. Returns playlist resource on success. Costs ~50 quota.";


YouTubePlaylistDetails::usage="YoutubePlaylistDetails[playlist] returns details about specified playlist. Gives 200 on success.";


YouTubeItem::usage="YoutubeItem[item] returns info on an item in a playlist. Costs ~50 quota.";


(* ::Text:: *)
(*With error stopping*)


YouTubeEmptyPlaylist::usage="YoutubeEmptyPlaylist[playlist] systematically deletes every item in playlist. Costs ~50 quota per item deleted.";


YouTubeAddManyToPlaylist::usage="YoutubeAddManyToPlaylist[playlist,videolist] tries to add many videos to a playlist, returning a list of what was unable to be added.";


(* ::Subsubsection:: *)
(*Diagnostic*)


YouTubeTestToken::usage="YouTubeTestToken[] tests acces_token.";


YouTubeVideoExistsQ::usage="YouTubeVideoExistsQ[video] checks if it exists.";


(* ::Section:: *)
(*Back end*)


Begin["`Private`"];


APIQ["YouTube"]=True;


(* ::Subsection:: *)
(*Login*)


LoginURL["YouTube",appdetails_Association]:=
Enclose[
Hyperlink["Youtube OAuth2 login",URLBuild[<|
	Method->"GET",
	"Scheme"->"https",
	"Domain"->"accounts.google.com",
	"Path"->"o/oauth2/v2/auth",
	"Query"->{
		"client_id"->Confirm@Lookup[appdetails,"client_id"],
		"redirect_uri"->"urn:ietf:wg:oauth:2.0:oob",
		"response_type"->"code",
		If[KeyMemberQ[appdetails,"scope"],
			"scope"->StringJoin[Riffle[Flatten[{Lookup[appdetails,"scope"]}]," "]],
			Message[LoginURL::scope];Nothing
		],
		"access_type"->"offline"
	}
|>]]
];


TokenRequest["YouTube",appdetails_Association,code_:None]:=
Enclose[
HTTPRequest[<|
	Method->"POST",
	"Scheme"->"https",
	"Domain"->"www.googleapis.com",
	"Path"->"/oauth2/v4/token",
	"ContentType"->"application/x-www-form-urlencoded",
	"Body"->Which[
	
	StringQ@code, (* use input code to get new access_token *){
		"grant_type"->"authorization_code",
		"code"->code,
		"client_id"->Confirm@Lookup[appdetails,"client_id"],
		"client_secret"->Confirm@Lookup[appdetails,"client_secret"],
		"redirect_uri"->"urn:ietf:wg:oauth:2.0:oob"
	},
	
	KeyExistsQ[appdetails,"refresh_token"], (* use refresh_token to get a new access_token *){
		"grant_type"->"refresh_token",
		"refresh_token"->Lookup[appdetails,"refresh_token"],
		"client_id"->Confirm@Lookup[appdetails,"client_id"],
		"client_secret"->Confirm@Lookup[appdetails,"client_secret"],
		"redirect_uri"->"urn:ietf:wg:oauth:2.0:oob"
	},
	
	Not@KeyExistsQ[appdetails,"scope"],
	Confirm[Failure["Input",<|"Message"->"YouTube does not allow unprivileged access."|>]],

	True,
	Confirm[Failure["Input",<|"Message"->"No code or refresh_token given for privileged access."|>]]
]
|>]
];


(* ::Subsection:: *)
(*Calls*)


$defaultyoutube={API->"YouTube"};


YouTubeSearch[search_String,type_String:"video",limit_Integer:5,return_String:"snippet",OptionsPattern[$defaultyoutube]]:=
APIRead[OptionValue[API],HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/search",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"part"->return,"type"->type,"q"->search,"maxResults"->ToString[limit]}
|>]];


YouTubeSearchReturnId[search_String,type_String:"video",limit_Integer:1,opts:OptionsPattern[$defaultyoutube]]:=
Lookup["videoId"]/@Lookup[Lookup[ExecuteReponse[YouTubeSearch[search,type,limit,"id",opts]],"items"],"id"];


YouTubeUpdatePlaylist[playlistid_String,title_String,description_String,OptionsPattern[$defaultyoutube]]:=
APIRead[OptionValue[API],
HTTPRequest[<|Method->"PUT",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlists",
"ContentType"->"application/json",
"Query"->{"part"->"snippet"},
"Body"->ExportString[{
"id"->playlistid,
"snippet"->{"title"->title,"description"->description}
},"JSON","Compact"->True]
|>]];


YouTubePlaylistItems[playlistid_String,OptionsPattern[$defaultyoutube]]:=
Module[{response,next,proceed},

Flatten[Reap[

response=ExecuteReponse[APIRead[OptionValue[API],
HTTPRequest[<|Method->"GET","Scheme"->"https","Domain"->"www.googleapis.com","Path"->"youtube/v3/playlistItems","ContentType"->"application/x-www-form-urlencoded",
"Query"->{
"part"->"id",
"playlistId"->playlistid,
"maxResults"->"50"
}
|>]]];
Sow[Lookup[#,"id"]&/@Lookup[response,"items"]];

While[KeyExistsQ[response,"nextPageToken"],
next=Lookup[response,"nextPageToken"];
response=ExecuteReponse[APIRead[OptionValue[API],
HTTPRequest[<|Method->"GET","Scheme"->"https","Domain"->"www.googleapis.com","Path"->"youtube/v3/playlistItems","ContentType"->"application/x-www-form-urlencoded",
"Query"->{
"part"->"id",
"playlistId"->playlistid,
"maxResults"->"50",
"pageToken"->next
}
|>]]];
Sow[Lookup[#,"id"]&/@Lookup[response,"items"]];

];

][[2,1]]]

];


YouTubeItem[itemid_String,OptionsPattern[$defaultyoutube]]:=
APIRead[OptionValue[API],
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlistItems",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"part"->"snippet","id"->itemid}
|>]]


YouTubeDeleteItem[itemid_String,OptionsPattern[$defaultyoutube]]:=
APIRead[OptionValue[API],
HTTPRequest[<|Method->"DELETE",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlistItems",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"id"->itemid}
|>]];


YouTubeDeletePlaylist[playlistid_String,OptionsPattern[$defaultyoutube]]:=
APIRead[OptionValue[API],
HTTPRequest[<|Method->"DELETE",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlists",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"id"->playlistid}
|>]];


YouTubeCreatePlaylist[title_String,description_String:"",OptionsPattern[$defaultyoutube]]:=
Module[{response},

response=APIRead[OptionValue[API],
HTTPRequest[<|Method->"POST",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlists",
"ContentType"->"application/json",
"Query"->{"part"->"snippet"},
"Body"->ExportString[{
"snippet"->{"title"->title,"description"->description}
},"JSON","Compact"->True]
|>]];

If[response["StatusCode"]==200,Lookup[ExecuteReponse@response,"id"],$Failed]

];


YouTubeAddToPlaylist[playlistid_String,videoid_String,position_Integer:Missing[],OptionsPattern[$defaultyoutube]]:=
APIRead[OptionValue[API],
HTTPRequest[<|Method->"POST",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlistItems",
"ContentType"->"application/json",
"Query"->{"part"->"snippet"},
"Body"->ExportString[{
"snippet"->{
"resourceId"->{"kind"->"youtube#video","videoId"->videoid},
"playlistId"->playlistid
}~Join~If[Not@MissingQ@position,{"position"->ToString[position]},{}]
},"JSON","Compact"->True]
|>]];


YouTubePlaylistDetails[playlistid_String,OptionsPattern[$defaultyoutube]]:=
APIRead[OptionValue[API],
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/playlists",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{
"part"->"contentDetails",
"id"->playlistid
}
|>]];


(* ::Subsubsection:: *)
(*Runs into quota limit*)


(* ::Text:: *)
(*Stops on first failure*)


YouTubeEmptyPlaylist[id_String,opts:OptionsPattern[$defaultyoutube]]:=
Module[{items,pause,n,i,code,read,proceed},
pause=0.1;
items=YouTubePlaylistItems[id,opts];

n=Length[items];
i=0;
PrintTemporary[Row[{ProgressIndicator[Dynamic[i/n]]," ",Dynamic[i],"/",n}]];

proceed=(n>0);
While[proceed,
Pause[pause];
read=YouTubeDeleteItem[items[[i+1]],opts];
code=read["StatusCode"];

If[Quotient[code,100]==2,i++,proceed=False];
If[i==n,proceed=False];
];

(* output remaining number of items *)
n-i

];


YouTubeAddManyToPlaylist[playlistid_String,videoidlist_List,opts:OptionsPattern[$defaultyoutube]]:=
Module[{n,i,code,read,proceed,pause},
pause=1;

n=Length[videoidlist];
i=0;
PrintTemporary[Row[{ProgressIndicator[Dynamic[i/n]]," ",Dynamic[i],"/",n}]];

proceed=(n>0);
While[proceed,
Pause[pause];
read=YouTubeAddToPlaylist[playlistid,videoidlist[[i+1]],opts];
code=read["StatusCode"];

If[Quotient[code,100]==2,i++,proceed=False];
If[i==n,proceed=False];
];

(* output remaining items to be added *)
videoidlist[[i+1;;]]

];


YouTubePlaylistItems[playlistid_String,OptionsPattern[$defaultyoutube~Join~{"FailOnIncomplete"->True}]]:=
Module[{response,next,proceed,list,read},

list=Flatten[Reap[

read=APIRead[OptionValue[API],
HTTPRequest[<|Method->"GET","Scheme"->"https","Domain"->"www.googleapis.com","Path"->"youtube/v3/playlistItems","ContentType"->"application/x-www-form-urlencoded",
"Query"->{
"part"->"id",
"playlistId"->playlistid,
"maxResults"->"50"
}
|>]];
proceed=(Quotient[read["StatusCode"],100]==2);
If[proceed,
response=ExecuteReponse[read];
Sow[Lookup[#,"id"]&/@Lookup[response,"items"]];
];

While[proceed \[And] KeyExistsQ[response,"nextPageToken"],
next=Lookup[response,"nextPageToken"];
read=APIRead[OptionValue[API],
HTTPRequest[<|Method->"GET","Scheme"->"https","Domain"->"www.googleapis.com","Path"->"youtube/v3/playlistItems","ContentType"->"application/x-www-form-urlencoded",
"Query"->{
"part"->"id",
"playlistId"->playlistid,
"maxResults"->"50",
"pageToken"->next
}
|>]];
proceed=(Quotient[read["StatusCode"],100]==2);
If[proceed,
response=ExecuteReponse[read];
Sow[Lookup[#,"id"]&/@Lookup[response,"items"]];
];

];

][[2,1]]];

(* output *)
If[proceed\[Or]Not[OptionValue["FailOnIncomplete"]],
list,
$Failed
]

];


(* ::Subsubsection:: *)
(*Diagnostic*)


YouTubeTestToken[OptionsPattern[$defaultyoutube]]:=ReadAuthorizedRequest[OptionValue[API],
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"oauth2/v3/tokeninfo",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{"access_token"->Lookup[API[OptionValue[API]],"access_token"]}
|>]
];


YouTubeVideoExistsQ[videoid_String,OptionsPattern[$defaultyoutube]]:=
APIReadExecute[OptionValue[API],
HTTPRequest[<|Method->"GET",
"Scheme"->"https",
"Domain"->"www.googleapis.com",
"Path"->"youtube/v3/videos",
"ContentType"->"application/x-www-form-urlencoded",
"Query"->{
"part"->"id",
"id"->videoid
}
|>],
(Lookup[Lookup[#,"pageInfo"],"totalResults"]>0)&
];


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
